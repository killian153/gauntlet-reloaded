#define _CRT_SECURE_NO_DEPRECATE
#include <Windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <tchar.h>
#pragma comment(lib,"winmm")
#include <locale.h>
#include "niveau.h"
#include "joueur.h"
#include "commun.h"
#include "musiques.h"
#include "gestionMenus.h"
#include "titres.h"


// Code ascii de la touche echap
#define TOUCHE_ESCAPE 27

int main(void)
{
	niveau_s  niveau;
	joueur_s  joueur;
	int       touche;
	int		  menu = 0;
	int		  testSon = 0;
	int		  niveauActuel = 1;
	int		  niveauTemp = 1;
	int		  gameover = 0;
	int		  best = 0;
	int       again = 0;
	int		  scoreTemp = 0;
	bool      tour_joueur_fini;
	char      str[100];
	int       invincible = 1;
	srand(time(NULL));

	SetConsoleTitle(_T("Gauntlet")); // Renommage de la console.

	sprintf(str, "mode con lines=%d cols=%d", HAUTEUR_ECRAN, LARGEUR_ECRAN); // Redimensionnement console
	system(str);

	do{

	launch();

	// Lecture du fichier audio
	if (testSon == 0)
	Jouer_Musique(0);

	menu = Accueil(); // Lancement menu

	if (menu != 0){

		do{
			menu = menuAccueil(menu, &joueur, &testSon, &invincible);

		} while (menu != 0);
	}

	system("cls");


	do{
		loading();
		Niveau_Constructeur(&niveau);
		Joueur_Constructeur(&joueur, invincible);

		Color_Choix(4);
		Locate(HAUTEUR_NIVEAU + 2, LARGEUR_NIVEAU + 6);
		if (niveauActuel == 1){Niveau_Charger(&niveau, "level1.txt");printf(TITRE_level1);}
		else if (niveauActuel == 2){Niveau_Charger(&niveau, "level2.txt");printf(TITRE_level2);}
		else if (niveauActuel == 3){Niveau_Charger(&niveau, "level3.txt"); printf(TITRE_level3);}
		else if (niveauActuel == 4){ Niveau_Charger(&niveau, "level4.txt"); printf(TITRE_level4);}
		else if (niveauActuel == 5){ Niveau_Charger(&niveau, "level5.txt"); printf(TITRE_level5); }

		Choix_Musique(niveauActuel, testSon);
		Niveau_Afficher(&niveau);
		Joueur_Afficher(&joueur);
		Joueur_AfficherInfo(&joueur);



		// Boucle de jeu principal
		do {
			// On commence par les actions du joueur : d�placements et tirs
			do {

				tour_joueur_fini = false;
				touche = _getch();
				// Action sur d�placement
				if (touche == DEP_HAUT || touche == DEP_BAS || touche == DEP_GAUCHE || touche == DEP_DROITE) {
					tour_joueur_fini = Joueur_Deplacer(&joueur, &niveau, touche, &niveauTemp, &scoreTemp);
				}
				// Action sur tir
				if (touche == TIR_HAUT || touche == TIR_BAS || touche == TIR_GAUCHE || touche == TIR_DROITE) {
					tour_joueur_fini = true;
					Joueur_Tir(&joueur, &niveau, touche);
				}
			} while (!tour_joueur_fini && touche != TOUCHE_ESCAPE);
			Joueur_AfficherInfo(&joueur);
			// On continue par les actions du niveau : d�placer monstres et spammer monstres
			Niveau_DeplacerMonstre(&niveau);
			// Si un des monstres s'est d�plac� sur le joueur
			if (niveau.decor[joueur.x][joueur.y] == MONSTRE_FANTOME) {
				Joueur_PrendDegat(&joueur, 150);
				Joueur_Afficher(&joueur);
			}
			else if (niveau.decor[joueur.x][joueur.y] == MONSTRE_SQUELETTE) {
				Joueur_PrendDegat(&joueur, 250);
				Joueur_Afficher(&joueur);
			}

			Niveau_SpammerMonstre(&niveau);
			// Le joueur perd de la vie � chaque tour
			Joueur_ActionFinDeTour(&joueur);
			Joueur_AfficherInfo(&joueur);


		} while (touche != TOUCHE_ESCAPE && Joueur_ToujoursVivant(&joueur) && niveauTemp == niveauActuel);


		niveauActuel += 1;
		system("cls");

	} while (touche != TOUCHE_ESCAPE && Joueur_ToujoursVivant(&joueur) && niveauActuel < 6);

	if (Joueur_ToujoursVivant(&joueur) == false || niveauActuel == 6){

		system("cls");

		if (testSon == 0)
			Jouer_Musique(6);

		comparaisonScore(scoreTemp, &best);

		gameover = gameOver(scoreTemp, best, &again);

		mciSendString(_T("stop mp3"), NULL, 0, 0);

		if (gameover != 0){again = 1;}


	}
	system("cls");

	} while (again == 0 && touche != TOUCHE_ESCAPE);

	return 0;
}
