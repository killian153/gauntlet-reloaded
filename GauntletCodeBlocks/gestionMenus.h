#ifndef H_GESTION // Protection de multi-inclusion.
#define H_GESTION



// Déclaration des variables.

void viderBuffer();
int MyGetch();
int codeTriche();
void launch();
int gameOver(int scoreFinal, int best, int *again);
int Accueil();
void Aide();
void Credits();
void classement();
int menuAccueil();
int comparaisonScore(int Score, int* ptrBest);

#endif