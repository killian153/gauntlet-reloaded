#ifndef H_COMMUN // Protection de multi-inclusion.
#define H_COMMUN

#define LARGEUR_ECRAN 180
#define HAUTEUR_ECRAN 45

#define LARGEUR_NIVEAU 110
#define HAUTEUR_NIVEAU 35

#define COIN_SUPERIEUR_GAUCHE_X (2)
#define COIN_SUPERIEUR_GAUCHE_Y ((LARGEUR_ECRAN - LARGEUR_NIVEAU) / 3)


typedef enum { false, true } bool;

void Locate(int x, int y);

void Color(int flags);

void Color_Choix(int choix);

void loading();

#endif
