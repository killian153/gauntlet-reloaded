#include "joueur.h"
#include "commun.h"
#include <stdio.h>
#include <windows.h>

// Permet de faire en sorte que 0 <= vie <= max_vie
void  Joueur_BorneVie(joueur_s *this)
{
	if (this->vie < 0) {
		this->vie = 0;
	}
	else if (this->vie > this->max_vie) {
		this->vie = this->max_vie;
	}
}

void  Joueur_Constructeur(joueur_s *this, int invincible)
{
	
	this->x = 2;
	this->y = 1;
	this->nb_tour_invulnerable = 0;
	this->nb_cle = 0;
	this->score = 0;
	if (invincible == 0){ this->vie = 100000; }
	else{ this->vie = 2000; }
	this->max_vie = this->vie;
	this->nb_vie = 0;

}

void  Joueur_Afficher(joueur_s *this)
{
	Locate(COIN_SUPERIEUR_GAUCHE_X + this->x, COIN_SUPERIEUR_GAUCHE_Y + this->y);
	Color_Choix(2);
	putchar('�');

}

void  Joueur_AfficherInfo(joueur_s *this)
{
	Color_Choix(0);
	Locate(15, 141);
	printf("Statistiques du joueur :\n");
	Locate(16, 141);
	printf("------------------------\n\n");
	Locate(18, 141);
	printf("\04 Invulnerable pendant "); Color_Choix(5); printf("%d", this->nb_tour_invulnerable); Color_Choix(0); printf(" tours\n");
	Locate(19, 141);
	printf("\04 Vie : "); Color_Choix(5); printf("%5d\n", this->vie); Color_Choix(0);
	Locate(20, 141);
	printf("\04 Score : "); Color_Choix(5); printf("%d\n", this->score); Color_Choix(0);
	Locate(21, 141);
	printf("\04 Nombre de cles : "); Color_Choix(5); printf("%d\n", this->nb_cle); Color_Choix(0);
	Locate(22, 141);
	printf("\04 Nombre de vies : "); Color_Choix(5); printf("%d\n", this->nb_vie); Color_Choix(0);
	Locate(0, 0);
	
}

bool  Joueur_Deplacer(joueur_s *this, niveau_s *niveau, char dir, int *niveauTemp, int *scoreTemp)
{
	int x;
	int y;

	if (dir != DEP_HAUT && dir != DEP_BAS && dir != DEP_GAUCHE && dir != DEP_DROITE) {
		return (false);
	}
	if (dir == DEP_GAUCHE || dir == DEP_DROITE) {
		x = this->x;
	}
	else if (dir == DEP_HAUT) {
		x = this->x - 1;
	}
	else {
		x = this->x + 1;
	}
	if (dir == DEP_HAUT || dir == DEP_BAS) {
		y = this->y;
	}
	else if (dir == DEP_GAUCHE) {
		y = this->y - 1;
	}
	else {
		y = this->y + 1;
	}
	// Ici, on va g�rer tous les actes que peut faire un joueur quand il arrive sur une case et faire la mise � jour
	
	
	switch (niveau->decor[x][y]) {
	case POTION_REGAIN_VIE_MINEUR: this->vie += (this->max_vie / 4); Joueur_BorneVie(this); break;
	case POTION_REGAIN_VIE_MAJEUR: this->vie += (this->max_vie / 2); Joueur_BorneVie(this); break;
	case POTION_REGAIN_VIE_TOTAL: this->vie = this->max_vie; break;
	case POTION_INVINCIBILITE: this->nb_tour_invulnerable += 21; break;
	case POTION_RESURRECTION: ++this->nb_vie; break;
	case CLE: ++this->nb_cle; break;
	case PORTE: if (this->nb_cle > 0) { --this->nb_cle; }
				else { return (false); } break;
	case COFFRE_FAIBLE_BUTIN: this->score += 100; *scoreTemp += 100; break;
	case COFFRE_FORT_BUTIN: this->score += 300; *scoreTemp += 300; break;
	case NOURRITURE_SODA: this->vie += 70; Joueur_BorneVie(this); break;
	case NOURRITURE_POULET: this->vie += 150; Joueur_BorneVie(this); break;
		// On ne peut sortir que si on a d�truit tous les g�n�rateurs et monstres
	case SORTIE: if (niveau->nb_generateurs > 0) { return (false); }else{ *niveauTemp += 1; } break;
	case GENERATEUR_FANTOME: return (false);
	case GENERATEUR_SQUELETTE: return (false);
	case MUR: return (false);
	case ENTREE: return (false);
	case MONSTRE_FANTOME: Joueur_PrendDegat(this, 150); break;
	case MONSTRE_SQUELETTE: Joueur_PrendDegat(this, 250); break;
	default: break;
	}
	// Si on arrive ici, cela veut dire que le d�placement est valide (m�me si on est mort)
	niveau->decor[x][y] = VIDE;
	// On affiche la case du niveau � la place de celle du joueur
	Niveau_AfficherCase(niveau, this->x, this->y);
	// On met � jour l'emplacement du joueur et on l'affiche
	this->x = x;
	this->y = y;
	Joueur_Afficher(this);
	return (true);
}

bool  Joueur_ToujoursVivant(joueur_s *this)
{
	if (this->vie == 0) {
		--this->nb_vie;
		if (this->nb_vie > 0) {
			this->vie = this->max_vie;
			return (true);
		}
		return (false);
	}
	return (true);
}

void  Joueur_PrendDegat(joueur_s *this, int degat)
{
	if (this->nb_tour_invulnerable == 0) {
		this->vie -= degat;
		Joueur_BorneVie(this);
	}
}

void  Joueur_Tir(joueur_s *this, niveau_s *niveau, char dir)
{
	// Le d�placement du projectile (li� � la direction)
	int dep_x;
	int dep_y;
	// La position du projectile
	int x;
	int y;

	// On v�rifie qu'on a bien une touche de tir (simple v�rification, par mesure de s�ret�
	if (dir != TIR_HAUT && dir != TIR_BAS && dir != TIR_GAUCHE && dir != TIR_DROITE) {
		return;
	}
	// On d�termine quelle direction va prendre le projectile
	if (dir == TIR_GAUCHE || dir == TIR_DROITE) {
		dep_x = 0;
	}
	else if (dir == TIR_HAUT) {
		dep_x = -1;
	}
	else {
		dep_x = 1;
	}
	if (dir == TIR_HAUT || dir == TIR_BAS) {
		dep_y = 0;
	}
	else if (dir == TIR_GAUCHE) {
		dep_y = -1;
	}
	else {
		dep_y = 1;
	}
	// La projection du tir est juste � c�t� du joueur
	x = this->x + dep_x;
	y = this->y + dep_y;
	// On avance le projectile jusqu'� ce qu'il rencontre un obstacle
	while (niveau->decor[x][y] == VIDE) {
		// On efface le projectile pr�c�dent
		Niveau_AfficherCase(niveau, x - dep_x, y - dep_y);
		// Si c'est le joueur qu'on vient d'effacer, on le r�affiche
		if (x - dep_x == this->x && y - dep_y == this->y) {
			Joueur_Afficher(this);
		}
		// On affiche le projectile
		Locate(COIN_SUPERIEUR_GAUCHE_X + x, COIN_SUPERIEUR_GAUCHE_Y + y);
		putchar('o');
		// On d�place le projectile
		x += dep_x;
		y += dep_y;
		// On mets en pause quelque temps, pour permettre de voir le projectile
		Sleep(20);
	}
	// On efface le projectile une derni�re fois
	Niveau_AfficherCase(niveau, x - dep_x, y - dep_y);
	// Si cet obstacle peut �tre d�truit, il l'est
	if (niveau->decor[x][y] == POTION_REGAIN_VIE_MINEUR ||
		niveau->decor[x][y] == POTION_REGAIN_VIE_MAJEUR ||
		niveau->decor[x][y] == POTION_REGAIN_VIE_TOTAL ||
		niveau->decor[x][y] == POTION_INVINCIBILITE ||
		niveau->decor[x][y] == POTION_RESURRECTION ||
		niveau->decor[x][y] == NOURRITURE_SODA ||
		niveau->decor[x][y] == NOURRITURE_POULET) {
		niveau->decor[x][y] = VIDE;
		Niveau_AfficherCase(niveau, x, y);
	}
	else if (niveau->decor[x][y] == MONSTRE_SQUELETTE || niveau->decor[x][y] == MONSTRE_FANTOME) {
		--niveau->nb_monstres;
		niveau->decor[x][y] = VIDE;
		Niveau_AfficherCase(niveau, x, y);
	}
	else if (niveau->decor[x][y] == GENERATEUR_SQUELETTE || niveau->decor[x][y] == GENERATEUR_FANTOME) {
		--niveau->nb_generateurs;
		niveau->decor[x][y] = VIDE;
		Niveau_AfficherCase(niveau, x, y);
	}
}

void  Joueur_ActionFinDeTour(joueur_s *this)
{
	// D�g�ts du joueur et invincibilit�
	Joueur_PrendDegat(this, 10);
	if (this->nb_tour_invulnerable > 0) {
		--this->nb_tour_invulnerable;
	}
}
