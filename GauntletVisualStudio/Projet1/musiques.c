#include <windows.h>
#include <tchar.h>



void Jouer_Musique(int directive){

	mciSendString(_T("stop mp3"), NULL, 0, 0);
	mciSendString(_T("close mp3"), NULL, 0, 0);

	switch (directive){

	case 0: mciSendString(_T("open arrival.mp3 alias mp3"), NULL, 0, 0);
		mciSendString(_T("play mp3 repeat"), NULL, 0, 0);
		break;

	case 1:	mciSendString(_T("open EncomPartII.mp3 alias mp3"), NULL, 0, 0);
		mciSendString(_T("play mp3 repeat"), NULL, 0, 0);
		break;

	case 2:	mciSendString(_T("open recognizer.mp3 alias mp3"), NULL, 0, 0);
		mciSendString(_T("play mp3"), NULL, 0, 0);
		break;

	case 3:	mciSendString(_T("open solarsailer.mp3 alias mp3"), NULL, 0, 0);
		mciSendString(_T("play mp3"), NULL, 0, 0);
		break;

	case 4:	mciSendString(_T("open flynnlives.mp3 alias mp3"), NULL, 0, 0);
		mciSendString(_T("play mp3"), NULL, 0, 0);
		break;

	case 5:	mciSendString(_T("open MindHeist.mp3 alias mp3"), NULL, 0, 0);
		mciSendString(_T("play mp3"), NULL, 0, 0);
		break;

	case 6:	mciSendString(_T("open endtitles.mp3 alias mp3"), NULL, 0, 0);
		mciSendString(_T("play mp3"), NULL, 0, 0);
		break;

	default: mciSendString(_T("stop mp3"), NULL, 0, 0);
		mciSendString(_T("close mp3"), NULL, 0, 0);
		break;

	}

}

void Choix_Musique(int niveauActuel, int testSon){

	if (niveauActuel == 1 && testSon == 0){
		Jouer_Musique(1);}
	if (niveauActuel == 2 && testSon == 0){
		Jouer_Musique(2);}
	else if (niveauActuel == 3 && testSon == 0){
		Jouer_Musique(3);}
	else if (niveauActuel == 4 && testSon == 0){
		Jouer_Musique(4);}
	else if (niveauActuel == 5 && testSon == 0)
		Jouer_Musique(5);}