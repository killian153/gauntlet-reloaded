#define _CRT_SECURE_NO_DEPRECATE
#include "niveau.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "windows.h"

void  Niveau_Constructeur(niveau_s *this)
{
	this->nb_monstres = 0;
	this->nb_generateurs = 0;
	this->tour = 0;
	
	this->aff_couleur[POTION_REGAIN_VIE_MINEUR] = FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[POTION_REGAIN_VIE_MAJEUR] = FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[POTION_REGAIN_VIE_TOTAL] = FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[POTION_INVINCIBILITE] = FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[POTION_RESURRECTION] = FOREGROUND_RED;
	this->aff_couleur[CLE] = FOREGROUND_BLUE | FOREGROUND_GREEN;
	this->aff_couleur[PORTE] = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[COFFRE_FAIBLE_BUTIN] = FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[COFFRE_FORT_BUTIN] = FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[NOURRITURE_SODA] = FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[NOURRITURE_POULET] = FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[MUR] = FOREGROUND_BLUE;
	this->aff_couleur[VIDE] = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[SORTIE] = FOREGROUND_GREEN | FOREGROUND_RED;
	this->aff_couleur[MONSTRE_SQUELETTE] = FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[MONSTRE_FANTOME] = FOREGROUND_RED;
	this->aff_couleur[GENERATEUR_SQUELETTE] = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[GENERATEUR_FANTOME] = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY;
	this->aff_couleur[ENTREE] = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY;

	this->aff_lettre[POTION_REGAIN_VIE_MINEUR] = '\03';//0
	this->aff_lettre[POTION_REGAIN_VIE_MAJEUR] = '\03';//1
	this->aff_lettre[POTION_REGAIN_VIE_TOTAL] = '\03';//2
	this->aff_lettre[POTION_INVINCIBILITE] = '\05';//3
	this->aff_lettre[POTION_RESURRECTION] = '\05';//4
	this->aff_lettre[CLE] = '�';//5
	this->aff_lettre[PORTE] = '|';//6
	this->aff_lettre[COFFRE_FAIBLE_BUTIN] = '\36';//7
	this->aff_lettre[COFFRE_FORT_BUTIN] = '\36';//8
	this->aff_lettre[NOURRITURE_SODA] = '\04';//9
	this->aff_lettre[NOURRITURE_POULET] = '\04';//10
	this->aff_lettre[MUR] = '�';//11
	this->aff_lettre[VIDE] = ' ';//12
	this->aff_lettre[SORTIE] = '['; //13
	this->aff_lettre[MONSTRE_SQUELETTE] = '�'; //14
	this->aff_lettre[MONSTRE_FANTOME] = '�'; //15
	this->aff_lettre[GENERATEUR_SQUELETTE] = 'O';//16
	this->aff_lettre[GENERATEUR_FANTOME] = 'O';//17
	this->aff_lettre[ENTREE] = '|'; //18
}

bool    Niveau_Charger(niveau_s *this, char *chemin_fichier)
{
	FILE  *fichier;
	int   i;
	int   j;

	if (!(fichier = fopen(chemin_fichier, "r"))) {
		perror("Niveau_Charger");
		return (false);
	}
	// R�cup�ration du d�cor
	for (i = 0; i < HAUTEUR_NIVEAU; ++i) {
		for (j = 0; j < LARGEUR_NIVEAU; ++j) {
			fscanf(fichier, "%d", (int*)(&this->decor[i][j]));
			if (this->decor[i][j] == GENERATEUR_FANTOME || this->decor[i][j] == GENERATEUR_SQUELETTE) {
				++this->nb_generateurs;
			}
			else if (this->decor[i][j] == MONSTRE_FANTOME || this->decor[i][j] == MONSTRE_SQUELETTE) {
				++this->nb_monstres;
			}
			fgetc(fichier);
		}
	}
	fclose(fichier);
	return (true);
}

void  Niveau_Afficher(niveau_s *this)
{
	int i;
	int j;

	for (i = 0; i < HAUTEUR_NIVEAU; ++i) {
		Locate(COIN_SUPERIEUR_GAUCHE_X + i, COIN_SUPERIEUR_GAUCHE_Y);
		for (j = 0; j < LARGEUR_NIVEAU; ++j) {
			Color(this->aff_couleur[this->decor[i][j]]);
			putchar(this->aff_lettre[this->decor[i][j]]);
		}
		putchar('\n');
	}
}

void  Niveau_AfficherCase(niveau_s *this, int i, int j)
{
	Locate(COIN_SUPERIEUR_GAUCHE_X + i, COIN_SUPERIEUR_GAUCHE_Y + j);
	Color(this->aff_couleur[this->decor[i][j]]);
	putchar(this->aff_lettre[this->decor[i][j]]);
}

#define MONSTRE_FANTOME_BOUGE   (NOMBRE_OBJET + MONSTRE_FANTOME)
#define MONSTRE_SQUELETTE_BOUGE (NOMBRE_OBJET + MONSTRE_SQUELETTE)

void  Niveau_DeplacerMonstre(niveau_s *this)
{
	int i;
	int j;
	int dep_x;
	int dep_y;

	if (this->nb_monstres > 0) {
		for (i = 0; i < HAUTEUR_NIVEAU; ++i) {
			for (j = 0; j < LARGEUR_NIVEAU; ++j) {
				if (this->decor[i][j] == MONSTRE_FANTOME) {
					switch (rand() % 4) {
					case 0: dep_x = 0; dep_y = -1; break;
					case 1: dep_x = 0; dep_y = 1; break;
					case 2: dep_x = 1; dep_y = 0; break;
					case 3: dep_x = -1; dep_y = 0; break;
					}
					if (this->decor[i + dep_x][j + dep_y] == VIDE) {
						this->decor[i + dep_x][j + dep_y] = MONSTRE_FANTOME_BOUGE;
						this->decor[i][j] = VIDE;
						Niveau_AfficherCase(this, i, j);
					}
				}
					if (this->decor[i][j] == MONSTRE_SQUELETTE) {
						switch (rand() % 4) {
						case 0: dep_x = 0; dep_y = -1; break;
						case 1: dep_x = 0; dep_y = 1; break;
						case 2: dep_x = 1; dep_y = 0; break;
						case 3: dep_x = -1; dep_y = 0; break;
						}
						if (this->decor[i + dep_x][j + dep_y] == VIDE) {
							this->decor[i + dep_x][j + dep_y] = MONSTRE_SQUELETTE_BOUGE;
							this->decor[i][j] = VIDE;
							Niveau_AfficherCase(this, i, j);
						}
					}
				
			}
		}
		for (i = 0; i < HAUTEUR_NIVEAU; ++i) {
			for (j = 0; j < LARGEUR_NIVEAU; ++j) {
				if (this->decor[i][j] == MONSTRE_FANTOME_BOUGE) {
					this->decor[i][j] = MONSTRE_FANTOME;
					Niveau_AfficherCase(this, i, j);
				}
				if (this->decor[i][j] == MONSTRE_SQUELETTE_BOUGE) {
					this->decor[i][j] = MONSTRE_SQUELETTE;
					Niveau_AfficherCase(this, i, j);
				}
			}
		}
	}
}

void  Niveau_SpammerMonstre(niveau_s *this)
{
	int i;
	int j;

	if (this->nb_generateurs) {
		++this->tour;
		// Spam les monstres tous les 4 tours
		if (!(this->tour % 30)) {
			for (i = 0; i < HAUTEUR_NIVEAU; ++i) {
				for (j = 0; j < LARGEUR_NIVEAU; ++j) {
					if (this->decor[i][j] == GENERATEUR_FANTOME) {
						if (this->decor[i - 1][j] == VIDE) {
							++this->nb_monstres;
							this->decor[i - 1][j] = MONSTRE_FANTOME;
							Niveau_AfficherCase(this, i - 1, j);
						}
						else if (this->decor[i][j - 1] == VIDE) {
							++this->nb_monstres;
							this->decor[i][j - 1] = MONSTRE_FANTOME;
							Niveau_AfficherCase(this, i, j - 1);
						}
						else if (this->decor[i + 1][j] == VIDE) {
							++this->nb_monstres;
							this->decor[i + 1][j] = MONSTRE_FANTOME;
							Niveau_AfficherCase(this, i + 1, j);
						}
						else if (this->decor[i][j + 1] == VIDE) {
							++this->nb_monstres;
							this->decor[i][j + 1] = MONSTRE_FANTOME;
							Niveau_AfficherCase(this, i, j + 1);
						}
					}
					else if (this->decor[i][j] == GENERATEUR_SQUELETTE) {
						if (this->decor[i - 1][j] == VIDE) {
							++this->nb_monstres;
							this->decor[i - 1][j] = MONSTRE_SQUELETTE;
							Niveau_AfficherCase(this, i - 1, j);
						}
						else if (this->decor[i][j - 1] == VIDE) {
							++this->nb_monstres;
							this->decor[i][j - 1] = MONSTRE_SQUELETTE;
							Niveau_AfficherCase(this, i, j - 1);
						}
						else if (this->decor[i + 1][j] == VIDE) {
							++this->nb_monstres;
							this->decor[i + 1][j] = MONSTRE_SQUELETTE;
							Niveau_AfficherCase(this, i + 1, j);
						}
						else if (this->decor[i][j + 1] == VIDE) {
							++this->nb_monstres;
							this->decor[i][j + 1] = MONSTRE_SQUELETTE;
							Niveau_AfficherCase(this, i, j + 1);
						}
					}
				}
			}
		}
	}
}
