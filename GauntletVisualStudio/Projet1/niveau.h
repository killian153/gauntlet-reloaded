#ifndef H_NIVEAU
#define H_NIVEAU

#include "commun.h"


typedef enum objet {
	POTION_REGAIN_VIE_MINEUR,
	POTION_REGAIN_VIE_MAJEUR,
	POTION_REGAIN_VIE_TOTAL,
	POTION_INVINCIBILITE,
	POTION_RESURRECTION,
	CLE,
	PORTE,
	COFFRE_FAIBLE_BUTIN,
	COFFRE_FORT_BUTIN,
	NOURRITURE_SODA,
	NOURRITURE_POULET,

	MUR,
	VIDE,
	SORTIE,
	MONSTRE_SQUELETTE,
	MONSTRE_FANTOME,
	GENERATEUR_SQUELETTE,
	GENERATEUR_FANTOME,
	ENTREE,
	NOMBRE_OBJET
} objet_s;

typedef struct niveau {

	objet_s decor[HAUTEUR_NIVEAU][LARGEUR_NIVEAU];
	// S'il faut d�truire tous les g�n�rateurs et tous les monstres..
	int     nb_monstres;
	int     nb_generateurs;
	// Permet de g�rer le spam des monstres.
	int     tour;
	// Utile pour l'affichage.
	char    aff_lettre[NOMBRE_OBJET];
	int     aff_couleur[NOMBRE_OBJET];

} niveau_s;

void  Niveau_Constructeur(niveau_s *this);
bool  Niveau_Charger(niveau_s *this, char *chemin_fichier);
void  Niveau_Afficher(niveau_s *this);
void  Niveau_AfficherCase(niveau_s *this, int i, int j);
void  Niveau_DeplacerMonstre(niveau_s *this);
void  Niveau_SpammerMonstre(niveau_s *this);

#endif
