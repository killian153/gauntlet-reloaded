#include "commun.h"

#include <windows.h>
#include <stdio.h>

void Locate(int x, int y)
{
#ifdef _WIN32
	HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD C;
	C.X = (SHORT)y;
	C.Y = (SHORT)x;
	SetConsoleCursorPosition(H, C);
#endif
	// Trouver alternative hors Windows.
}

void Color(int flags)
{
#ifdef _WIN32
	HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(H, (WORD)flags);
#endif
}

// Vider buffer
void viderBuffer()
{
	int c = 0;
	while ((c = getchar()) != '\n' && c != EOF);
}

void Color_Choix(int choix){

	switch (choix){

	case 0: Color(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY); break; // Blanc
	case 1: Color(FOREGROUND_BLUE | FOREGROUND_INTENSITY); break; // Bleu
	case 2: Color(FOREGROUND_GREEN | FOREGROUND_INTENSITY); break; // Vert
	case 3: Color(FOREGROUND_RED | FOREGROUND_INTENSITY); break; // Rouge
	case 4: Color(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY); break; // Cyan
	case 5: Color(FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY); break; // Jaune
	case 6: Color(FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY); break; // Violet
	case 7: Color(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY); break;
	case 8: Color(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED); break; // Blanc sombre
	case 9: Color(FOREGROUND_RED); break; // Rouge sombre
	case 10: Color(FOREGROUND_BLUE | FOREGROUND_GREEN); break;
	default: break;
	}
}
