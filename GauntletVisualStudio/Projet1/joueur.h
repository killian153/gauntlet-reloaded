#ifndef H_JOUEUR
#define H_JOUEUR

#include "niveau.h"

#define DEP_HAUT    'z'
#define DEP_BAS     's'
#define DEP_GAUCHE  'q'
#define DEP_DROITE  'd'

#define TIR_HAUT    'i'
#define TIR_BAS     'k'
#define TIR_GAUCHE  'j'
#define TIR_DROITE  'l'

typedef struct joueur {
	int x;
	int y;
	int score;                // En relation avec les coffres
	int vie;                  // En relation avec certaines potions, la nourriture, les monstres
	int max_vie;              // Utilis� lors d'une r�surrection ou bien lors d'une prise de PV
	int nb_tour_invulnerable; // Si == 0, vuln�rable, sinon donne le nombre de tour encore invuln�rable
	int nb_vie;               // Augmente gr�ce aux potions et permet de ressusciter
	int nb_cle;
	
} joueur_s;


void  Joueur_Constructeur(joueur_s *this, int invincible);
void  Joueur_Afficher(joueur_s *this);
void  Joueur_AfficherInfo(joueur_s *this);
bool  Joueur_Deplacer(joueur_s *this, niveau_s *niveau, char dir, int *niveauTemp, int *scoreTemp);
bool  Joueur_ToujoursVivant(joueur_s *this);
void  Joueur_PrendDegat(joueur_s *this, int degat);
void  Joueur_Tir(joueur_s *this, niveau_s *niveau, char dir);
void  Joueur_ActionFinDeTour(joueur_s *this);

#endif
