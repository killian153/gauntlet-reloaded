#define _CRT_SECURE_NO_DEPRECATE
#include "titres.h"
#include "joueur.h"
#ifdef _WIN32
#include <windows.h>
#include <conio.h>
#endif
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#pragma comment(lib,"winmm")
#include <locale.h>
#include "commun.h"
#include "gestionMenus.h"
#include "joueur.h"



// Fonction récupération de la touche.
int MyGetch()
{
#ifdef _WIN32
	return _getch();
#endif
	// Trouver alternative hors Windows.
}


// Fonction gameOver avec highscore
int gameOver(int scoreFinal, int best, int *again){

	char nom[100];
	FILE *nomJoueur = NULL;
	char effacement[] = "    *			";
	int choix = 2;

	Sleep(3200);

	Color_Choix(3);
	printf(TITRE_gameover);


	printf("\n");

	Color_Choix(0);
	printf("\n\n\n\n\n");
	Locate(20, 81);
	printf("VOTRE SCORE EST ");
	Color_Choix(2);
	printf("%d\n\n\n", scoreFinal);

	if (best == 1){
		printf("								    Nouveau record ! Entrez votre pseudo : ");
		nomJoueur = fopen("nomMeilleurJoueur.txt", "w+");
		Color_Choix(5);
		scanf("%s", &nom);

		fprintf(nomJoueur, "%s", nom);
		printf("\n\n\n");
	}
	if (best != 1){
		printf("\n\n\n\n");
	}
	Color_Choix(0);
	printf("										       0. Rejouer\n");
	printf("										       ----------\n");
	printf("										       1. Quitter\n\n\n");
	printf("										         ******\n");
	printf("										         *    *\n");
	printf("										         ******\n");

	while (choix != 0 || choix != 1){
		Color_Choix(0);
		Locate(33, 90);
		printf("%s", effacement);
		Color_Choix(2);
		Locate(33, 90);
		scanf("%d", &choix);
		return choix;
	}

	return choix;

}


// Fonction Accueil
int Accueil(){

	int choix = 0;
	char effacement[] = "    ";

	Color_Choix(4);
	printf(TITRE_accueil);

	printf("\n");

	Color_Choix(0);
	printf("\n\n\n\n\n");
	printf("										       0. JOUER\n");
	printf("										       ----------\n");
	printf("										       1. Aide\n");
	printf("										       ----------\n");
	printf("										       2. Credits\n");
	printf("										       ----------\n");
	printf("										       3. Classement\n\n");
	printf("									   -------------------------------------\n");
	printf("									   4. Couper le son // 5. Activer le son\n\n\n");
	printf("											 ******\n");
	printf("											 *    *\n");
	printf("											 ******\n");
	Locate(30, 90);
	Color_Choix(2);

	while (scanf("%d", &choix) != 1){
		viderBuffer();
		Locate(30, 90);
		printf("%s", effacement);
		Locate(30, 90);
		
	}

	return choix;
}


// Fonction Aide
void Aide(){

	Color_Choix(4);
	printf(TITRE_aide);
	printf("\n");

	printf("\n\n");
	Color_Choix(8);
	printf("   Bienvenue dans Gauntlet ! Vous incarnez, Sam, un magicien qui tente de nettoyer\n\n");
	printf("   le donjon d'Ulgard des forces du mal, a travers 5 niveaux a la difficulte croissante.\n\n");
	printf("   Dans ce jeu ou chaque deplacement compte, vous devrez venir a bout des monstres grace\n\n");
	printf("   a vos pouvoirs magiques (a utiliser avec moderation).\n\n   Dans votre quete, vous serez aide par ");
	printf("diverses potions et vous aurez la possibilite\n\n   de ramasser des tresors, qui ne feront ");
	printf("qu'accroitre davantage votre richesse (et votre\n\n   reputation de magicien qui aime l'argent!)\n\n\n");
	printf("                                    Bonne chance !\n");
	Locate(15, 105);
	Color_Choix(2);
	printf("\04 Deplacements : "); Color_Choix(0); printf("touches zqsd"); Color_Choix(5); printf(" / "); Color_Choix(2); printf("Sorts : "); Color_Choix(0); printf("touches ijkl");
	Locate(17, 105);
	Color_Choix(2);
	printf("\04 Potions : "); Color_Choix(3); printf("\03"); Color_Choix(5); printf(" / "); Color_Choix(2); printf("Potions invulnerabilite et resurrection : "); Color_Choix(3); printf("\05");
	Locate(19, 105);
	Color_Choix(2);
	printf("\04 Tresors : "); Color_Choix(5); printf("\36");
	Locate(21, 105);
	Color_Choix(2);
	printf("\04 Nourriture : "); Color_Choix(6); printf("\04");
	Locate(23, 105);
	Color_Choix(2);
	printf("\04 Cles : "); Color_Choix(10); printf("§");
	Locate(25, 105);
	Color_Choix(2);
	printf("\04 Squelettes : "); Color_Choix(3); printf("²"); Color_Choix(5); printf(" / "); Color_Choix(2); printf("Fantomes : "); Color_Choix(9); printf("²");
	Locate(27, 105);
	Color_Choix(2);
	printf("\04 Generateurs de monstres : "); Color_Choix(0); printf("O");
	Color_Choix(4);
	Locate(38, 60);
	printf("-> Appuyer sur une touche pour revenir au menu precedent...");
}


// Fonction Credits
void Credits(){


	Color_Choix(4);
	printf(TITRE_credits);

	printf("\n");

	printf("\n\n");
	Color_Choix(3);
	printf("							PROJET REALISE PAR :				MUSIQUES :\n");
	Color_Choix(0);
	printf("							--------------------				----------\n\n");
	printf("							Killian MELLE & Loic NIEDERST			Tron Legacy & Inception Soundtracks\n\n\n");
	Color_Choix(3);
	printf("							A L'AIDE DE :\n");
	Color_Choix(0);
	printf("							-------------\n\n");
	printf("							Microsoft Visual Studio 2013 & Atom\n\n\n");
	Color_Choix(3);
	printf("							REMERCIEMENTS A :\n");
	Color_Choix(0);
	printf("							-----------------\n\n");
	printf("							Mme Zineb MEHEL\n");
	Locate(35, 20);
	Color_Choix(4);
	printf("					     -> Appuyer sur une touche pour revenir au menu precedent...");
}


// Fonction classement
void classement(){

	int scoreTemp = 0;
	char nom[100];

	FILE *scoreJoueur = NULL;
	FILE *nomJoueur = NULL;

	scoreJoueur = fopen("scoreMeilleurJoueur.txt", "r");
	fscanf(scoreJoueur, "%d", &scoreTemp);
	fclose(scoreJoueur);

	nomJoueur = fopen("nomMeilleurJoueur.txt", "r");
	fscanf(nomJoueur, "%s", &nom);
	fclose(nomJoueur);

	Color_Choix(4);
	printf(TITRE_highscore);

	printf("\n\n\n\n\n\n");
	Color_Choix(0);
	printf("							    Le meilleur score est detenu par ");
	Color_Choix(5);
	printf("%s ", nom);
	Color_Choix(0);
	printf("avec un score de ");
	Color_Choix(5);
	printf("%d\n\n\n\n", scoreTemp);
	Color_Choix(4);
	printf("							     -> Appuyer sur une touche pour revenir au menu precedent...");
}


// Fonction démarrage
void launch(){

	int i;
	Locate(16, 58);

	char launcher[63] = {
		"² Pour une meilleure experience de jeu, activez votre son... ²"
	};

	for (i = 0; i < 63; i++){

		Color_Choix(0);
		printf("%c", launcher[i]);
		Sleep(15);

	}
	printf("\n");
	Sleep(2000);
	system("cls");

}


// Fonction loading
void loading(){

	int i;

	char barre[63] = {
		"² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ² ²"};
	Locate(13,79);
	Color_Choix(2);
	printf("Chargement en cours...");
	Locate(16, 63);
	for (i = 0; i < 63; i++){

		Color_Choix(0);
		printf("%c", barre[i]);
		Sleep(15);

	}
	printf("\n");
	Sleep(1200);
	system("cls");

}


// Fonction menu secret
int codeTriche(){

	char code_secret[] = "estaca2015";
	char code_quitter[] = "0";
	char code_utilisateur[80];

	Color_Choix(4);

	do{

		system("cls");

		Locate(15, 75);
		printf("²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²");
		Locate(16, 75);
		printf("²                             ²");
		Locate(17, 75);
		printf("²   Entrez le code secret :   ²");
		Locate(18, 75);
		printf("²                             ²");
		Locate(19, 75);
		printf("²  ->                         ²");
		Locate(20, 75);
		printf("²                             ²");
		Locate(21, 75);
		printf("²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²²");
		Locate(25, 74);
		printf("-> Tapez 0 pour revenir au menu...");

		Locate(19, 82);
		gets(code_utilisateur);

		if (strcmp(code_quitter, code_utilisateur) == 0){
			return 1;
		}

	} while (strcmp(code_secret, code_utilisateur) != 0);

	return 0;

}


// Fonction Menu
int menuAccueil(int menu, joueur_s *this, int* TestSon, int *invincible){

	int touche;
	char effacement[] = "  ";
	int life = 0;

	do { // On ne démarre pas le jeu tant que..

		if (menu == 1){

			system("cls");
			Aide();
			touche = MyGetch();

			if (touche != 27){ // Si l'utilisateur appuie sur une touche, on revient au menu.

				system("cls");
				menu = Accueil();
			}
		}else if (menu == 2){

				system("cls");
				Credits();
				touche = MyGetch();

				if (touche != 27){

					system("cls");
					menu = Accueil();
				}

			}else if (menu == 3){

				system("cls");
				classement();
				touche = MyGetch();

				if (touche != 27){

					system("cls");
					menu = Accueil();
				}
			}else if (menu == 4){

				mciSendString(_T("stop mp3"), NULL, 0, 0);
				*TestSon = 1;

				Locate(30, 90);
				printf("%s", effacement);
				Color_Choix(2);
				Locate(30, 90);
				scanf("%d", &menu);

			if (menu == 5){

				mciSendString(_T("play mp3 repeat"), NULL, 0, 0);
				*TestSon = 0;

				}
		}else if (menu == 6 && *invincible != 0){

				system("cls");
				*invincible = codeTriche(); // Menu secret.

			if (*invincible == 0){

				Locate(25, 74);
				printf("                                  ");
				Color_Choix(6);
				Locate(25, 67);
				printf("Code correct ! Vous etes desormais invincible !");
				Color_Choix(0);
				Locate(28, 62);
				printf("-> Appuyer sur une touche pour revenir au menu precedent...");
				*invincible = 0;
				touche = MyGetch();

				if (touche != 27){ // Si l'utilisateur appuie sur une touche, on revient au menu.
					system("cls");
					menu = Accueil();

				}
			}

			if (*invincible == 1){ // Menu secret.
				system("cls");
				menu = Accueil();
			}

		}else if (menu == 6 && *invincible == 0){ // Alerte invincibilité.

			Locate(35, 60);
			Color_Choix(5);
			printf("/!\\ Invincibilite active. Tapez 99 si vous souhaitez la desactiver.");
			Locate(30, 90);
			printf("%s", effacement);
			Color_Choix(2);
			Locate(30, 90);
			scanf("%d", &menu);

			if (menu == 99){ // Désactive l'invincibilité.

				*invincible = 1;
				Locate(30, 90);
				printf("%s", effacement);
				
			}
		} else {

			system("cls");
			menu = Accueil();
		}

	} while (menu != 0);

	return 0;
}


// Le score est-il meilleur que le highscore ?
int comparaisonScore(int Score, int* Best){

	int scoreTemp = 0;

	FILE *scoreJoueur = NULL;

	scoreJoueur = fopen("scoreMeilleurJoueur.txt", "r");


	fscanf(scoreJoueur, "%d", &scoreTemp);
	fclose(scoreJoueur);

	if (scoreTemp >= Score){
		*Best = 0;
	}
	else if (scoreTemp < Score){

		scoreJoueur = fopen("scoreMeilleurJoueur.txt", "w+");
		fprintf(scoreJoueur, "%d", Score);
		fclose(scoreJoueur);

		*Best = 1;

	}

	return 0;

}
